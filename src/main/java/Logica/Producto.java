package Logica;

import BaseDatos.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Producto
{
    private String id;
    private String nombre;
    private double temperatura;
    private double valorBase;
    public Object Double;
    
    public Producto(String id,String nombre, double temperatura, double valorBase) {
        this.id = id;
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public Producto() {
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }
  
    @Override
    public String toString() {
        return this.getClass().getName() + "{" + "id=" + id + ", nombre=" + nombre + ", temperatura=" + temperatura + ", valorBase=" + valorBase + '}';
    }
    
    public boolean guardarProducto()
    {
        ConexionBD conexion = new ConexionBD();
        String sql = "INSERT INTO productos(id,nombre,temperatura,valorBase)"
                +"VALUES('"+this.id+"','"+this.nombre+"','"+this.temperatura+"','"+this.valorBase+"')";
        if(conexion.setAutoCommitBD(false))
        {
            if(conexion.insertarBD(sql))
            {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            }
            else
            {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else
        {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean actualizarProducto()
    {
        ConexionBD conexion = new ConexionBD();
        String sql ="UPDATE productos SET nombre='"+this.nombre+"',temperatura='"
                +this.temperatura+"',valorBase='"+this.valorBase+"'WHERE id="+this.id;
        
        if(conexion.setAutoCommitBD(false))
        {
            if(conexion.actualizarBD(sql))
            {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            }
            else 
            {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        }
        else 
        {
            conexion.cerrarConexion();
            return false;
        }
        
    }
    public boolean eliminarProducto(String IdProducto)
    {
       ConexionBD conexion = new ConexionBD();
       String sql ="DELETE FROM productos WHERE id="+ IdProducto;
       System.out.println("Sentencia para borrar "+sql);
       if(conexion.setAutoCommitBD(false))
       {
           if(!conexion.borrarBD(sql))
           {
               conexion.rollbackBD();
               conexion.cerrarConexion();
               return false;
           }
           else
           {
               conexion.commitBD();
               conexion.cerrarConexion();
               return true;
           }
       }
       else
       {
           conexion.cerrarConexion();
           return false;
       }
    }
    public List<Producto> listarProductos() 
    {
        List<Producto> listaProductos = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Producto p; 
            while (rs.next()) 
            {
                p = new Producto();
                p.setId(rs.getString("id"));
                p.setNombre(rs.getString("nombre"));
                p.setTemperatura(rs.getDouble("temperatura"));
                p.setValorBase(rs.getDouble("valorBase"));
                listaProductos.add(p);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }
   
}

