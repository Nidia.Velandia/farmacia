package Presentacion;

import Logica.Producto;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class vPrincipal extends javax.swing.JFrame {
    String titulos[] = {"Id", "Nombre", "Temperatura", "Valor"};
    String fila[] = new String[4];
    DefaultTableModel modelo;
    
    public vPrincipal() {
        initComponents();
        Producto obj = new Producto();
        List<Producto> lista = obj.listarProductos();
        
        modelo = new DefaultTableModel(null,titulos);
        
        for(Producto i: lista)
        {
            fila[0]= i.getId();
            fila[1]= i.getNombre();
            fila[2]= String.valueOf(i.getTemperatura());
            fila[3]= String.valueOf(i.getValorBase());
            modelo.addRow(fila);
        }
        vTabla.setModel(modelo);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        vId = new javax.swing.JLabel();
        vNombre = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        vId1 = new javax.swing.JTextField();
        vNombre1 = new javax.swing.JTextField();
        vTemperatura1 = new javax.swing.JTextField();
        vValorBase1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        vTabla = new javax.swing.JTable();
        vGuardar = new javax.swing.JButton();
        vActualizar = new javax.swing.JButton();
        vEliminar = new javax.swing.JButton();
        vConsultar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        vId.setText("Id");

        vNombre.setText("Nombre");

        jLabel3.setText("Temperatura");

        jLabel4.setText("Valor Base");

        vTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(vTabla);

        vGuardar.setText("Guardar");
        vGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vGuardarActionPerformed(evt);
            }
        });

        vActualizar.setText("Actualizar");
        vActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vActualizarActionPerformed(evt);
            }
        });

        vEliminar.setText("Eliminar");
        vEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vEliminarActionPerformed(evt);
            }
        });

        vConsultar.setText("Consultar");
        vConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                vConsultarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(vGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(vActualizar, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)
                            .addComponent(vEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(vConsultar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(vId)
                            .addComponent(vNombre))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(vId1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(vNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(vTemperatura1)
                            .addComponent(vValorBase1))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(vId)
                    .addComponent(vId1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(vNombre)
                    .addComponent(vNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(vTemperatura1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(vValorBase1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(32, 32, 32)
                .addComponent(vGuardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(vActualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(vEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(vConsultar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void vGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vGuardarActionPerformed
        Producto obj =new Producto();
        obj.setId(vId1.getText());
        obj.setNombre(vNombre1.getText());
        obj.setTemperatura(Double.valueOf(vTemperatura1.getText()));
        obj.setValorBase(Double.valueOf(vValorBase1.getText()));
        
        if(obj.guardarProducto())
        {
           JOptionPane.showMessageDialog(this,"Producto guardado con exito\n"+obj);
        }
        else
        {
           JOptionPane.showMessageDialog(this,"Error al guardar datos en BD?\n"+obj);
        }    
    }//GEN-LAST:event_vGuardarActionPerformed

    private void vActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vActualizarActionPerformed
        // TODO add your handling code here:
        this.dispose();
        vModificar ventana = new vModificar ();
        ventana.setVisible(true);
        
    }//GEN-LAST:event_vActualizarActionPerformed

    private void vConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vConsultarActionPerformed
        // TODO add your handling code here:
        vMostrar ventana =new vMostrar();
        ventana.setVisible(true);
    }//GEN-LAST:event_vConsultarActionPerformed

    private void vEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_vEliminarActionPerformed
        // TODO add your handling code here:
        this.dispose();
        vEliminar ventana = new vEliminar ();
        ventana.setVisible(true);
    }//GEN-LAST:event_vEliminarActionPerformed

    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new vPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton vActualizar;
    private javax.swing.JButton vConsultar;
    private javax.swing.JButton vEliminar;
    private javax.swing.JButton vGuardar;
    private javax.swing.JLabel vId;
    private javax.swing.JTextField vId1;
    private javax.swing.JLabel vNombre;
    private javax.swing.JTextField vNombre1;
    private javax.swing.JTable vTabla;
    private javax.swing.JTextField vTemperatura1;
    private javax.swing.JTextField vValorBase1;
    // End of variables declaration//GEN-END:variables
}
